//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	/* {"⌨", "sb-kbselect", 			0, 	30}, */
	{"", "cat /tmp/recordingicon 2>/dev/null",	0,	9},
	{"",	"sb-task",				10,	13},
	/* {"",	"sb-music",				0,	11}, */
	{"",	"sb-news",				0,	6},
	{"",	"sb-mailbox",				60,	12},
	{"",	"sb-torrent",				5,	7},
	{"",	"sb-pacpackages",			0,	8},
	{"",	"sb-crypto-bank ADA Cardano 💰",	60,	2},
	{"",	"sb-crypto-index",			600,	22},
	{"",	"sb-price BTC Bitcoin ",		60,	21},
	/* {"",	"sb-cpu",				10,	18}, */
	/* {"",	"sb-cpubars",				10,	15}, */
	/* {"",	"sb-moonphase",				600,	17}, */
	{"",	"sb-forecast",				600,	5},
	{"",	"sb-doppler",				600,	23},
	{"",	"sb-memory",				1,	14},
	{"",	"sb-volume",				0,	10},
	{"",	"sb-battery",				5,	3},
	{"",	"sb-clock",				1,	1},
	/* {"",	"sb-nettraf",				1,	16}, */
	{"",	"sb-internet",				1,	4},
};

//sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = "|";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
